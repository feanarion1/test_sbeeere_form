const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const isProduction = process.env.NODE_ENV === 'production';


module.exports = {
  devtool: isProduction ? undefined : "source-map",
  mode: isProduction ? 'production' : 'development',

  entry: "./src/index.js",
  output: {
    path: path.join(__dirname, "/dist"),
    filename: "index-bundle.js"
  },
  resolve: {
    alias: {
      '@ui-core': path.resolve(__dirname, 'src/components/ui-core'),
      '@store': path.resolve(__dirname, 'src/core/store'),
      '@assets': path.resolve(__dirname, 'src/assets'),
      "@styles" :path.resolve(__dirname, 'src/styles'),
     },
    extensions: [ ".js", ".jsx"]

  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: ["babel-loader"]
      },
      {
        test: /\.(le|c)ss$/,
        use: [
          isProduction ? MiniCssExtractPlugin.loader : 'style-loader',
          { loader: 'css-loader', options: { importLoaders: 1 } },
          'postcss-loader',
          'less-loader'
        ],
      },
    
      {
        test: /\.(png|jp(e*)g|gif)$/,
        use: [{
          loader: 'url-loader',
          options: {
            limit: 8000, // Convert images < 8kb to base64 strings (but, can i ned this for svg?)
            name: '[hash]-[name].[ext]',
            outputPath: 'images/'
          }
        }]
      },
    
      {
        test: /\.svg$/,
        exclude: /assets\/svgr/,
        use: {
          loader: "file-loader",
          options: {
            name: '[hash]-[name].[ext]',
            outputPath: 'images/'
          }
        }
      },
      {
        test: /\.(eot|woff(2)?|ttf|otf)(\?\S*)?$/,
        use: {
          loader: "file-loader",
          options: {
            name: "[name].[contenthash].[ext]",
            outputPath: "fonts/"
          }
        }
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./src/index.html"
    })
  ]
};