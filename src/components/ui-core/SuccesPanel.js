import React, { useState } from 'react';
import Icon from '@ui-core/Icon';
import "@styles/SuccesPanel.less"

export default function SuccessPanel(props) {
  return (
    <div className='success-panel'>
      <Icon className='success-logo' name='icon-ok' />
      <div className='success-message'>
        {"Заявка отправлена! Менеджер уже звонит, посмотрите на телефон!"}
      </div>
    </div>
  );
}
