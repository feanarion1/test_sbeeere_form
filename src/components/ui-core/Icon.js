import React from 'react';

/**  ----КОМПОНЕНТ ICON---- */
export default function Icon(props) {
  return (
    <div
      style={props.style}
      onClick={props.onClick}
      className={`
        ${`icon_container`} 
        ${props.name ? props.name : ''}
        ${props.className || ''}
        `}
    />
  );
}
