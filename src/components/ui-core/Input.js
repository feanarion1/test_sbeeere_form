import React, { useEffect, useState } from 'react';
import "@styles/input.less"

export default function Input(props) {
  const [val, setVal] = useState('');
  const [error, setError] = useState(false);
  useEffect(() => {
    setTimeout(() => {
      setError(false);
    }, 2500);
  }, [error]);

  const { type, placeholder } = props;
  return (<>
    <input
      style={{
        borderBottom: error ? '1px solid red' : '1px solid rgb(160, 160, 160)',
        width: props.width ? props.width : ''
      }}
      className='input'
      value={val || ''}
      type={type}
      placeholder={error ? 'Полне не должно быть пустым!' : placeholder}
      onBlur={e => {
        // if (props.placeholder.includes('*')) {
        //   setError(true);
        //   return '';
        // }
        // setError(false);
        props.onBlur(e.target.value);
        return '';
      }}
      onChange={event => {
        if (!event.target.value.match(props.pattern)) {
          setError(true)
          return ''
        }
        props.onBlur(event.target.value);
        setVal(event.target.value)
      }
      }
    />
    {<div style={{ height: '24px', color: 'red', textAlign: 'left', fontSize: '12px' }}>{error ? props.warning : ''}</div>}
  </>
  );
}
