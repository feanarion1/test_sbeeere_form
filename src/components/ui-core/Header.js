import React from 'react';
import "@styles/header.less"

export default function Header(props) {
  const { text } = props;
  return <div className='header'>{text}</div>;
}
