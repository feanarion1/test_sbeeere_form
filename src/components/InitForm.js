import React, { useEffect, useState } from 'react';
import { connect, useDispatch } from 'react-redux';
import Header from '@ui-core/Header';
import Input from '@ui-core/Input';
import SuccessPanel from '@ui-core/SuccesPanel';
import {
  sendLogin,
  sendPhone,
  sendMail,
  sendSubmitted,
  clearData
} from '@store/actions/actions';
import '@styles/init.less';

function InitForm(props) {
  const dispatch = useDispatch();
  // const [success, setSuccess] = useState(false);

  const { className, name, phone, email } = props;


  function enterFunction(event) {
    if (event.keyCode === 13) {
      name && phone && handleClick()

    }
  }

  useEffect(() => {
    // componentWillUnmount
    document.addEventListener("keyup", enterFunction, false)

    return () => {
      document.removeEventListener("keyup", enterFunction, false);
    }
  }, [handleClick]);

  function handleClick() {
    // const url = 'https://api.github.com/repos/javascript-tutorial/en.javascript.info/commits';

    // props.name == 'админ' && props.phone == '1234' ? setSuccess(true) : setSuccess(false)
    (name && phone) ? dispatch(sendSubmitted(true)) : dispatch(sendSubmitted(false))
    // props.submitted ? dispatch(sendSubmitted(true)) : dispatch(sendSubmitted(false))
  }
  useEffect(() => {
    props.submitted && setTimeout(() => {
      dispatch(sendSubmitted(false));
      dispatch(clearData())
    }, 5000);
  }, [props.submitted])


  return (<div className='init-form_container'>
    <div className={`init-form ${className}`}>
      {props.submitted ? (
        <SuccessPanel />
      ) : (
        <div style={{ border: '1px solid gray' }}>
          <Header text='Заголовок формы' />
          <div style={{ height: '0px' }} />
          <div className='input-container'>
            <Input
              pattern={/^([а-яА-Я]||\s)+$/}
              warning={'Принимаются только кириллица и пробелы'}
              value={name}
              placeholder='Имя*'
              onBlur={value => dispatch(sendLogin(value))}
            />
            <Input

              width={'150px'}
              pattern={/^\+?\d*$/}
              warning={'Только цифры и знак +'}

              value={phone}
              placeholder='Номер телефона*'
              onBlur={value => dispatch(sendPhone(value))}
            />
            <Input
              value={email}
              placeholder='Электронная почта'
              onBlur={value => dispatch(sendMail(value))}
            />
          </div>
          <button type='button' className='submit-button' style={{ backgroundColor: name && phone ? 'black' : 'gray' }} onClick={() => name && phone && handleClick()}>
            {"Начать работу"}
          </button>
          <p className='descr'>
            {"Нажимая кнопку «Начать работу», я даю свое согласия на обработку персональных данных."}

            <a href={'./README.md'} style={{ color: 'green' }} download>
              {"Условия использования данных"}
            </a>
          </p>
        </div>
      )}
    </div>
  </div>
  );
}

const mapStateToProps = state => {
  return {
    phone: state.phone,
    name: state.login,
    email: state.email,
    submitted: state.submitted
  };
};

export default connect(mapStateToProps)(InitForm);
