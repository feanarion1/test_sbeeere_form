import React, { Component } from "react";
import store from "@store/store";
import { Provider } from 'react-redux'
import InitForm from "./InitForm";
import '@styles/App.less';
import "../fontello/css/fontello.css"


class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <InitForm />
      </Provider>
    );
  }
}


export default App;