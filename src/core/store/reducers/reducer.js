
const initialState = {
  login: "",
  email: "",
  phone: "",
  submitted: false

};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case "SEND_LOGIN":
      return {
        ...state,
        login: action.value
      };
    case "SEND_MAIL":
      return {
        ...state,
        email: action.value
      };
    case "SEND_PHONE":
      return {
        ...state,
        phone: action.value
      };
    case "SEND_SUBMITTED":
      return {
        ...state, 
        submitted: action.value
      }
      case "CLEAR_DATA":
        return {
          login: "",
          email: "",
          phone: "",
          submitted: false
        }
    default:
      return state
  }
};

export default reducer