const SEND_LOGIN = 'SEND_LOGIN';
const SEND_PHONE = "SEND_PHONE"
const SEND_MAIL = 'SEND_MAIL';
const SEND_SUBMITTED = 'SEND_SUBMITTED';
const CLEAR_DATA  = "CLEAR_DATA"

const sendLogin = value => ({
  type: SEND_LOGIN,
  value,
});


const sendPhone = value => ({
  type: SEND_PHONE,
  value
});
const sendMail = value => ({
  type: SEND_MAIL,
  value
});

const sendSubmitted = value => ({
  type: SEND_SUBMITTED,
  value
})

const clearData = () => ({
  type: CLEAR_DATA,

})


export {
  SEND_LOGIN,
  SEND_PHONE,
  SEND_MAIL,
  SEND_SUBMITTED,
  CLEAR_DATA,
  sendLogin,
  sendPhone,
  sendMail,
  sendSubmitted,
  clearData
}